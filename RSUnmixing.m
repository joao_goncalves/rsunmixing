function RSUnmixing(MOD_rst, MOD_spres, LT_rst, LT_rst_pred, LT_rst_class, LT_ncols, LT_nrows, LT_spres, nb, mws, LT_pred_unmix)

% ------------------------------------------------------------------------- %
%
% Writeen by Xu Yong  updated version 2015
% The Chinese University of Hong Kong
%
% Refer to the following paper:
% Xu Y.,Huang B., Cao K., Guo C & Meng D. Spatial and Temporal Image Fusion
% via Regularized Spatial Unmixing. IEEE Geoscience and Remote Sensing
% Letters, 12(6):1362-1366, 2015.
%
% ------------------------------------------------------------------------- %

if(ischar(LT_ncols))
    LT_ncols = str2double(LT_ncols);
end

if(ischar(LT_nrows))
    LT_nrows = str2double(LT_nrows);
end

if(ischar(LT_spres))
    LT_spres = str2double(LT_spres);
end

if(ischar(MOD_spres))
    MOD_spres = str2double(MOD_spres);
end

if(ischar(nb))
    nb = int16(str2double(nb));
end

if(ischar(mws))
    mws = str2double(mws);
end

pad_size = double(9);

col = double(round((LT_ncols * LT_spres) / MOD_spres));
row = double(round((LT_nrows * LT_spres) / MOD_spres));

col1=LT_ncols;
row1=LT_nrows;

bands = nb;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Reconstructed Modis data

fp=fopen(MOD_rst,'r');

for i=1:bands
    [Modis_t1(:,:,i),~]=fread(fp,[col1,row1],'int16');
end
fclose(fp);

Modis_t1 = Modis_t1/10000; % scale factor -> convert to decimal

for k=1:bands
    for i=1:col1
        for j=1:row1
            ModisH(i,j,k)=Modis_t1(j,i,k);
        end
    end
end

for k=1:bands
    ModisHH(:,:,k) = padarray(ModisH(:,:,k),double([pad_size*col1/col,pad_size*row1/row]),'symmetric','both');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Unknown Modis data

ratio_x=row1/row;
ratio_y=col1/col;

for nn=1:bands
    for i=1:col
        for j=1:row
            ratio_x1=round((i-1)*ratio_x)+1;
            ratio_x2=round(i*ratio_x);
            ratio_y1=round((j-1)*ratio_y)+1;
            ratio_y2=round(j*ratio_y);
            Modis(j,i,nn)=mean(mean(ModisH(ratio_y1:ratio_y2,ratio_x1:ratio_x2,nn)));
        end
    end
end

for k=1:bands
    ModisLL(:,:,k)=padarray(Modis(:,:,k),[pad_size,pad_size],'symmetric','both');
end

clear Modis;
clear ModisH;
clear Modis_t1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Simulated landsat data

fp=fopen(LT_rst_pred,'r');

for i=1:bands
    [landsat1(:,:,i),~]=fread(fp,[col1,row1],'int16');
end
fclose(fp);

landsat1=landsat1/10000;

for k=1:bands
    for i=1:col1
        for j=1:row1
            Simulate_Landsat(i,j,k)=landsat1(j,i,k);
        end
    end
end

for k=1:bands
    Real_Landsatnew(:,:,k)=padarray(Simulate_Landsat(:,:,k),double([pad_size*col1/col,pad_size*row1/row]),'symmetric','both');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Real landsat data

fp=fopen(LT_rst,'r');

for i=1:bands
    [landsat1(:,:,i),~]=fread(fp,[col1,row1],'int16');
end
fclose(fp);

landsat1=landsat1/10000;

for k=1:bands
    for i=1:col1
        for j=1:row1
            Real_Landsat(i,j,k)=landsat1(j,i,k);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Classified landsat data

fp=fopen(LT_rst_class,'r');

[classimg_t,~]=fread(fp,[col1,row1],'uchar');
fclose(fp);

for i=1:col1
    for j=1:row1
        classimg(i,j)=classimg_t(j,i);
    end
end

classimgnew=padarray(classimg,double([pad_size*col1/col,pad_size*row1/row]),'symmetric','both');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Unmixing

Nc = max(max(classimgnew));
[endm,~,endx] = calculate1(classimgnew,Real_Landsatnew,col+2*pad_size, row+2*pad_size, col1+2*pad_size*col1/col, row1+2*pad_size*row1/row);

endm = reshape(endm,col+2*pad_size, row+2*pad_size, Nc);
endx = reshape(endx,col+2*pad_size, row+2*pad_size, Nc, bands);

Nb = mws;
Predict_Landsat=unmixing(ModisLL, endx, classimgnew, endm, Nb, ModisHH, pad_size);% unchanged
Predict_Landsatnew=Predict_Landsat(1+pad_size*col1/col:pad_size*col1/col+col1,1+pad_size*row1/row:pad_size*row1/row+row1,:);


for k=1:bands
    for i=1:col1
        for j=1:row1
            Predict_Landsat1(i,j,k)=Predict_Landsatnew(j,i,k);
        end
    end
end

% Write output predicted file from linear unmixing
for i= 1:bands
    Predicted=floor(Predict_Landsat1(:,:,i)*10000+0.5);
    filename=strcat(LT_pred_unmix,'_b',num2str(i));
    fid=fopen(filename,'w');
    fwrite(fid,Predicted,'int16');
    fclose(fid);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [endm, classB, endx] = calculate1(classA,Landsat,col, row, ratiox,ratioy)

endNum=max(max(classA));

classB = Landsat;
[~,~,bLR]=size(Landsat);

endm=zeros(col*row,endNum);
endx=zeros(col*row,endNum,bLR);
meanx=zeros(endNum,bLR);

total=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ratio_x=ratiox/col;
ratio_y=ratioy/row;

for i=1:col
    for j=1:row
        
        total=total+1;
        sumA=zeros(1,endNum);
        ratio_x1=round((i-1)*ratio_x)+1;
        ratio_x2=round(i*ratio_x);
        ratio_y1=round((j-1)*ratio_y)+1;
        ratio_y2=round(j*ratio_y);
        sumsum=0;
        meanx(1:endNum,1:bLR)=0;
        
        for k=ratio_x1:ratio_x2
            for l=ratio_y1:ratio_y2
                sumsum=sumsum+1;
                for c=1:endNum
                    if classA(l,k)==c
                        sumA(c)=sumA(c)+1;
                        for b=1:bLR
                            meanx(c,b)=meanx(c,b)+Landsat(l,k,b);
                        end
                    end
                end
            end
        end % end for k
        
        for c=1:endNum
            if sumA(c)>0
                meanx(c,:)=meanx(c,:)/sumA(c);
            end
        end
        
        endm(total,:)=sumA/sumsum;
        endx(total,:,:)=meanx;
        
        for k=ratio_x1:ratio_x2
            for l=ratio_y1:ratio_y2
                classB(l,k,:)= meanx(classA(l,k),:);
            end
        end
    end
end



function HR = unmixing(LR,endvalue,classifidimg,endm,neig,HR, neig1)

% LR: lower image
% Classifiedimg: Higher image
% Neig: Neig*Neig neighbour pixels to be considered for each iterative
% calculate

Nc = max(max(classifidimg));
[nxLR, nyLR,bLR] = size(LR);
sumPPP = zeros(Nc,bLR);
sumPP = zeros(Nc,1);
[nxHR, nyHR] = size(classifidimg);
nxS = nxHR/nxLR;
nyS = nyHR/nyLR;

K = (2*neig+1)*(2*neig+1);

% Make progress bar
wait = waitbar(0,'Pixel unmixing ...');

loop = 1;
iter = nxLR-2*neig-1;

sumPPP(1:Nc,1:bLR) = 0;
left(1:Nc,1:bLR) = 0;
right(1:Nc,1:bLR) = 0;

% fmincon option set
opts = optimset('fmincon');
opts.Display = 'off';
opts.Diagnostics = 'off';
opts.Algorithm = 'sqp'; % generally faster than interior-point (default)
opts.MaxIter = 500;

for i=neig1+1:nxLR-neig1
    for j=neig1+1:nyLR-neig1
        waitbar(loop/(iter*iter),wait);
        F=[];
        
        for m=1:Nc
            Ftmp=endm(i-neig: i+neig, j-neig: j+neig,m);
            F=[F Ftmp(:)];
            xy_index = find(Ftmp>0);
            
            for b=1:bLR
                Fvalue=endvalue(i-neig: i+neig, j-neig: j+neig,m,b);
                
                if(length(xy_index)>=1)
                    sumPPP(m,b)=mean(Fvalue(xy_index));
                    left(m,b)=min(Fvalue(xy_index));
                    right(m,b)=max(Fvalue(xy_index));
                else
                    sumPPP(m,b) = 0;
                    left(m,b) = 0;
                    right(m,b) = 0;
                end
            end
        end
        
        tmpPixel=[];
        
        for b=1:bLR
            Pixels=LR(i-neig: i+neig, j-neig: j+neig, b);% Modis vector
            sumPP = sumPPP(:,b);
            
            %% For constrained mode
            
            P=Pixels(:);
            y0=pinv(F'*F)*F'*P;
            yno= fmincon(@(x) (F*x-P)'*(F*x-P)+0.1*K*(x-sumPP)'*(x-sumPP)/Nc,y0,[],[],[],[],[1e-6*ones(Nc,1) ones(Nc,1)-1e-6*ones(Nc,1)],[],[],opts);      %
            tmpPixel=[tmpPixel yno];
            
            %% For constrained mode
        end
        
        ratio_x1=round((i-1)*nxS)+1;
        ratio_x2=round(i*nxS);
        ratio_y1=round((j-1)*nyS)+1;
        ratio_y2=round(j*nyS);
        
        for k1=ratio_x1:ratio_x2
            for k2=ratio_y1:ratio_y2
                HR(k1,k2,:)= tmpPixel(classifidimg(k1,k2),:)';
            end
        end
        %%
        loop=loop+1;
    end
end

close(wait);
