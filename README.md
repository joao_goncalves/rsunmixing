## RSUnmixing ##

A CLI version of the image fusion algoritm in Xu et al., 2015 (Spatial and Temporal Image Fusion 
via Regularized Spatial Unmixing, IEEE GEOSCIENCE AND REMOTE SENSING LETTERS, VOL. 12, NO. 6, 
JUNE 2015) implemented on Matlab

Binaries are available for Windows x64 using Matlab R2015a / MRC-8.5 version   

Matlab code developed and provided by Yong Xu and Yuyue Xu    
Adaptations to Matlab CLI by João Gonçalves 

### Abstract ###

A novel spatial and temporal data fusion model
based on regularized spatial unmixing was developed to generate
Landsat-like synthetic data with the fine spatial resolution of
Landsat Enhanced Thematic Mapper Plus (Landsat ETM+) data
and the high temporal resolution of Moderate Resolution Imaging
Spectroradiometer (MODIS) data. The proposed approach is
based on the conventional spatial unmixing technique, but modified
to include prior class spectra, which are estimated from pairs
of MODIS and Landsat data using the spatial and temporal adaptive
reflectance data fusion model. The method requires the optimization
of the following three parameters: the number of classes
of Landsat data, the neighborhood size of the MODIS data for
spatial unmixing, and a regularization parameter added to the cost
function to reduce unmixing error. Indexes of relative dimensionless
global error in synthesis (ERGAS) were used to determine the
best combination of the three parameters by evaluating the quality
of the fused result at both Landsat and MODIS spatial resolutions.
The experimental results with observed satellite data showed
that the proposed approach performs better than conventional
unmixing-based fusion approaches with the same parameters.


### Parameters ###
(use these in order)   

1. **MOD_rst**: Path to MODIS raster with the same spatial resolution of Landsat   
2. **MOD_spres**: MODIS original spatial resolution (e.g, 250, 500 or 1000)    
3. **LT_rst**: Path to the original Landsat raster    
4. **LT_rst_pred**: Path to the STARFM predicted Landsat raster    
5. **LT_rst_class**: Path to the classified Landsat raster    
6. **LT_ncols**: number of columns in the Landsat raster    
7. **LT_nrows**: number of rows in the Landsat raster    
8. **LT_spres**: spatial resolution of Landsat data (tipically: 30)    
9. **nb**: number of bands in MODIS and Landsat data    
10. **mws**: moving windows size (final size is set to 2*mws + 1)    
11. **LT_pred_unmix**: Path to the output predicted raster (note: this data file has no header!!)
     
**Example:**    
RSUnmixing C:/data/modis2000306 1000 C:/data/tm2000306 C:/data/pre_tm2000306 C:/data/tm2000306_30class 1200 1200 30 3 5 C:/data/tm2000306_pred_unmix
